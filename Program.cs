﻿using System.Text;
using System.IO;
using System;
using System.Diagnostics;
using Microsoft.Win32;

// Минимальные требования от FW 4.0 - (клиент) Windows XP Home Edition и (сервер) Windows Small Business Server 2003 Standard Edition
// взято отсюда - http://msdn.microsoft.com/library/8z6watww.aspx
// .NET Framework (клиентский профиль) не поддерживается в системах на платформе IA-64(Itanium)
//


namespace OperaSerchINIRepairer
{
    class Program
    {
        static void Main(string[] args)
        {

            string startPath=string.Empty;
            // Проверяем параметры командной строки
            if (!(args.Length == 0))
            {
                if (args[0] == "/?" || args[0] == "?" || args[0] == "-?" || args[0] == "/help" || args[0] == "-help")
                {
                    // Показываем HELP и выходим
                    DisplayHelp();
                }
                else if (args[0].ToLower().Contains("-start"))
                {
                    startPath = args[0].ToLower().Substring(7);
                }
            }

            /*
            // Производим замену стандартных ярлыков Opera на ярлыки OperaSerchINIRepairer
            if (args[0].ToLower() == "-install")
            {
                string temp1 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "opera.lnk");
                if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "opera.lnk")))
                {
                    
                    Console.WriteLine("DesktopDirectory="+Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "opera.lnk"));
                }
                string temp2 = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu), "opera.lnk");
                if (File.Exists(Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu), "opera.lnk")))
                {
                    
                    Console.WriteLine("StartMenu="+Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.StartMenu), "opera.lnk"));
                }
                Environment.Exit(0);
            }
            */

            // Сразу проверяем запущена ли Opera, если запущена то менять файл не имеет смысла
            Process[] processes = Process.GetProcessesByName("opera");
            if (processes.Length > 0)
            {
                WrireWarningStr("Сейчас все процессы OPERA будут закрыты");
                //WrireWarningStr("НАЖМИТЕ ЛЮБУЮ КЛАВИШУ ДЛЯ ПРОДОЛЖЕНИЯ");
                //Console.ReadKey();
                foreach (Process cur_proc in processes)
                {
                    try
                    {
                        cur_proc.Kill();
                    }
                    catch (System.Exception)
                    {

                        throw;
                    }

                }

            }
            
            // Определяем путь где установлена Opera из реестра
            RegistryKey hkcu = Registry.CurrentUser;
            hkcu = hkcu.OpenSubKey("Software\\Opera Software");
            Object obp = hkcu.GetValue("Last Install Path");
            Console.WriteLine("Opera установлена в {0}", obp);
           
            // Проверяем параметр Multi User в operaprefs_default.ini
            string operaDir = obp.ToString();
            string searchIniFile = String.Empty;
            string operaIniFile = Path.Combine(operaDir, "operaprefs_default.ini");
            Console.WriteLine("Читаю файл настроек {0}", operaIniFile);
            Console.WriteLine("Пытаюсь найти параметр Multi User");
            foreach (string line in File.ReadLines(operaIniFile, Encoding.UTF8))
            {
                if (line.Contains("Multi User"))
                {
                    //Console.BufferWidth = 100; // Чтобы вся строка влезла на экран без переносов
                    //Console.WriteLine(line);
                    if (line.Contains("=1") || line.Contains("= 1"))
                    {
                        searchIniFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "opera\\opera\\search.ini");
                        Console.WriteLine("Multi User=1");
                    }
                    else if (line.Contains("=0") || line.Contains("= 0"))
                    {
                        searchIniFile = Path.Combine(obp.ToString(), "search.ini");
                        Console.WriteLine("Multi User=0");
                    }
                    else
                    {
                        WrireErrorStr("Для Multi User не определен ни 0 ни 1");
                    }
                }
            }
            
            // В Opera 11.60 не было параметра Multi User
            if (searchIniFile == "")
            {
                searchIniFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "opera\\opera\\search.ini");
                Console.WriteLine("Параметр Multi User не был определен, путь по умолчанию ", searchIniFile);
            }

            // Изменяем файл search.ini
            if (!File.Exists(searchIniFile))
            {
                WrireErrorStr("Не могу найти файл " + searchIniFile);
            }
            Console.WriteLine("Читаю файл {0}", searchIniFile);
            string text = File.ReadAllText(searchIniFile, Encoding.UTF8);
            text = text.Replace("koi8-r", "windows-1251");
            Console.WriteLine("Сохраняю файл {0}", searchIniFile);
            File.WriteAllText(searchIniFile, text, Encoding.UTF8);
            //WrireWarningStr("НАЖМИТЕ ЛЮБУЮ КЛАВИШУ ДЛЯ ВЫХОДА");
            //Console.ReadKey();
            
            // Запускаем Opera заново
            if (startPath == string.Empty)
            {
                if (File.Exists(Path.Combine(operaDir, "opera.exe")))
                {
                    Process.Start(Path.Combine(operaDir, "opera.exe"));
                }
                else
                {
                    WrireWarningStr("Opera на запускается. Не найден файл "+Path.Combine(operaDir, "opera.exe"));
                }
            }
            else
            {
                if (File.Exists(startPath))
                {
                    Process.Start(startPath);
                }
                else
                {
                    WrireWarningStr("Opera на запускается. Не найден файл " + startPath);
                }
            }

        }

        static void WrireErrorStr(string errorstr)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(errorstr);
            Console.ReadKey();
            Console.ResetColor();
            Environment.Exit(1);
        }
        static void WrireWarningStr(string warningstr)
        {
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine(warningstr);
            //Console.ReadKey();
            Console.ResetColor();
        }

        private static void DisplayHelp()
        {

            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Справка по параметрам запуска:");
            Console.WriteLine("OperaSerchINIRepairer.exe <-команда>");
            Console.WriteLine("  <-команда>:");
            Console.WriteLine("     -start - Запуск opera.exe (нужно указывать полный путь в двойных кавычках (\"C:\\Program Files (x86)\\Opera\\opera.exe\")");
            Environment.Exit(0);
        }
    }
}
